# 镜像文件
FROM golang:latest

# 维修者 维护者
# MAINTAINER JosonJiang "87592776@qq.com"
# MAINTAINER has been deprecated

# 镜像中项目路径
WORKDIR $GOPATH/src/JosonJiang.com/josongolang
# 拷贝当前目录代码到镜像
COPY . $GOPATH/src/JosonJiang.com/josongolang
# 制作镜像
RUN go build .

# 暴露端口
EXPOSE 8000

# 程序入口
ENTRYPOINT ["./josongolang"]
