package main
import "fmt"
import "time"

import (
	"net/http"
	"runtime"
	"strings"
)

func main() {

	// fmt.Println("\n")
	// var now time.Time
	// now=  time.Now()
	// fmt.Println( now)

	// fmt.Println( time.Now().Format("2006-01-02 15:04"))
	fmt.Println(runtime.Version())

	var strbuild strings.Builder

	strbuild.WriteString(" \n\n \n\n Hello Golang from google \n\r ")
	strbuild.WriteString("现在时间：" )
	strbuild.WriteString( time.Now().String() )
	strbuild.WriteString( "\n\r 现在时间：" + time.Now().Format("2006-01-02 15:04"))
	strbuild.WriteString( "\n\r Go Version:" + runtime.Version())


	//注册一个函数，响应某一个路由
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(" Hello Golang from Joson \n\r " + time.Now().String() + "\n\n " + time.Now().Format("2006-01-02 15:04") + "\n\r Version:" + runtime.Version()))
		w.Write([]byte(strbuild.String()))
	})

	//启动端口服务
	http.ListenAndServe(":8000", nil)

}



//参考 https://blog.csdn.net/niyuelin1990/article/details/79035728

//func main() {
//
//	v, ok := syscall.Getenv("TASKID")
//	log.Println("Getenv", v, ok)
//
//	e := echo.New()
//	e.GET("/", handlerindex)
//	log.Println("starting echo")
//	err := e.Start(":8000")
//	if err != nil {
//		log.Fatal("echo", err)
//	}
//
//
//}
//
//func handlerindex(c echo.Context) error {
//
//	log.Println("hello world handlerindex")
//	return c.JSON(http.StatusOK, `{"hello":"world"}`)
//
//}