  #   - echo "Execute this script instead of the global after_script." 

<<EOF


    - echo $PWD
    - echo $CI_JOB_NAME
    - echo $CI_PROJECT_DIR
    - echo $CI_COMMIT_REF_NAME

    - echo "Execute this script in all jobs that don't already have an after_script section."

EOF

#!/bin/bash


     echo "执行 after_script 脚本查看变量开始"
     echo "Execute this script in all jobs that don't already have an after_script section."

     echo "测试网络是否正常 curl http://www.baidu.com"

     curl -sL -w "%{http_code}\\n" "http://www.baidu.com" -o /dev/null
     if [[ 200 == $? ]]; then  echo "可以访问互联网"; else  echo "网络好像出了点问题..." ; fi

     echo $PWD

     echo "执行 after_script 脚本查看变量结束"