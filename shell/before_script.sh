<<EOF
    - echo $PWD
    - echo $CI_JOB_NAME
    - echo $CI_PROJECT_DIR
    - echo $CI_COMMIT_REF_NAME



    - echo Hello World
    - echo "before_script 开始 编译 $CI_JOB_NAME-$CI_COMMIT_REF_NAME"

    - docker --version

    - ls
    - pwd
    - echo $PWD
    - echo ${PWD}

    - echo  $PWD > logs/on_failure.log 
    - pwd > logs/on_success.log
    - docker info > logs/on_success.log
    - docker info > logs/on_failure.txt


    echo "传入值"
    
    echo $1
    echo "$1"

    containerName=$1

    echo $containerName
    echo "接收到传入值" ${containerName}


EOF

#!/bin/bash

     echo "before_script 开始 编译 $CI_JOB_NAME-$CI_COMMIT_REF_NAME"
     echo $PWD
     echo $CI_JOB_NAME
     echo $CI_COMMIT_REF_NAME
     echo $CI_PROJECT_DIR

     echo  $PWD > logs/on_failure.log 

     #/home/gitlab-runner/logs/on_success.log
    #  pwd > ~/logs/on_success.log
     #/home/gitlab-runner/logs/on_success.log: No such file or directory

     docker info > logs/on_success.log
     docker info > ./logs/on_success.log
     docker info > $PWD/logs/on_success.log
     docker info > $PWD/logs/on_failure.txt


    #检测执行脚本的用户
    if [ "$(whoami)" != 'root' ]; then

        echo " `whoami` You have no permission to run $0 as non-root user. $(whoami)"
        #exit 1;

        echo "`whoami`"
        echo "$(whoami)"  
      
        echo `whoami`
        echo $(whoami)

        pwd
        echo ${PWD}

    fi

 
    containerName=$1
    echo " 接收到传入  值 =>$1 $containerName >" ${containerName}



    #当前时间
    now=`date +"%Y-%m-%d %H:%M:%S"`
    # 查看进程是否存在
    Running=`docker inspect --format '{{.State.Running}}' ${containerName}`


    SymbolicLink="/var/log/docker"
    DockerLogDir=" ${PWD}/logs/docker"

    if [ -d "$DockerLogDir" ] ; then 

        #rm -rf $DockerLogDir

       echo "删除 ${DockerLogDir} 文件夹"
    else
       echo "创建 ${DockerLogDir} 文件夹"

        #mkdir -p ${DockerLogDir}
        #mkdir -p /var/log/docker/
        mkdir -p ${DockerLogDir} 2> /dev/null

        # 这是一个软连接
        # ln -s ${DockerLogDir} /var/log/
        # rm -rf ${SymbolicLink}

        #if [ $? == 0 ] ; then
        if [[ ! -x "${DockerLogDir}" || $? == 0 ]] ; then

            pwd

            chmod +x ${DockerLogDir}
            #chmod 777 /var/log/docker/

        fi
       
    fi



 
    monitorlog="${DockerLogDir}/docker_monitor.log"

    if [ ! -f "${DockerLogDir}/docker_monitor.log" ] ; then 
     
     touch ${DockerLogDir}/docker_monitor.log
     chmod +x ${DockerLogDir}/docker_monitor.log

    else 
        echo "文件 ${DockerLogDir}/docker_monitor.log 存在"

        if [ ! -x "$monitorlog"]; then
            echo "无权限 ${monitorlog} "
            chmod +x "${monitorlog}"
        else
            echo "有权限 ${monitorlog}"
           
        fi

    fi


    #ln -s file1 link1
    #ln -s -fb ${PWD}/logs/docker/ /var/log/
    #ln -s /home/gitlab-runner/builds/FE7T3pz9/0/JosonJiang/Golang/logs/docker/  /var/log/
    #容器启动时可以挂载该目录 在宿主机上查看相关日志

    if [ "${Running}" != "true" ]; then

        docker start ${containerName}
        #echo "${now} 重启docker容器，容器名称：${containerName}"

        #记录日志
        echo "${now} 重启docker容器，容器名称：${containerName}" >> ${PWD}/logs/docker/docker_monitor.log
    else

        docker stop ${containerName}
        #echo "${now} 关闭docker容器，容器名称：${containerName}"
        echo "${now} 关闭docker容器，容器名称：${containerName}" >> ${PWD}/logs/docker/docker_monitor.log

    fi


     echo "执行脚本查看变量结束"