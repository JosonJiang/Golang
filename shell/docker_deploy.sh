<<EOF

     - docker info
     - echo "This script executes first. When it completes, the job's [after_script] executes."


EOF

#!/bin/bash

    echo "This script 【$0】 executes first. When it completes, the job's [after_script] executes."
    
    imageName=$3
    containerName=$1
    containerPort=$2

    echo "接收到参数 $1 < containerName : ${containerName}, $3 < imageName : ${imageName}"

    # imageName="golangs"  
    # containerName="josongolang"


    # 删除 TAG  <none>  的容器 判断
    # echo "删除TAG <none>的容器"

    if [[ -n `docker images | grep 'none'| awk '{print $3}' 2> /dev/null` || -n `docker images -q -f dangling=true 2> /dev/null` ]] ; then
          
          echo "删除TAG <none>的容器"
          docker rmi `docker images | grep "none" | awk '{print $3}'`
    else      
          echo "没有TAG <none>的容器"
    fi



    # 停止容器  
    if [[ -n "`docker ps -aq --filter "name=${containerName}"`"  ||  -n "`docker ps -a  | grep "${containerName}" | wc -l`" ]] ; then 
    # if [[ -n "`docker ps -aq --filter "name=josongolang"`"  ||  -n "`docker ps -a  | grep "josongolang" | wc -l`" ]] ; then 
        echo "停止容器"       
        docker stop ${containerName}
     
    else      
        echo "没有容器"
    fi
 
    # 删除容器
    if [[ -n "`docker images -aq "${imageName}"`" ]]; then  
    # if [[ -n "`docker images -aq "golangs"`" ]]; then  
        echo "删除容器"; 
        docker rm -f ${containerName}
    else  
        echo "没有容器" 
    fi

    
    # # 查看进程是否存在
    # exist=`docker inspect --format '{{.State.Running}}' ${containerName}`
    # if [ "${exist}" == "true" ]; then
    #     docker stop ${containerName}
    #     docker rm -f  ${containerName}
    # fi



    #删除镜像
    if [[ "$(docker images -q ${imageName}:latest 2> /dev/null)" != "" ]]; then 
       echo ”删除镜像“ 
       docker rmi ${imageName}
    else
       echo ”没有镜像“; 
    fi


    # 通过Dockerfile生成 golangs 镜像
    docker build -t ${imageName} .

    # 通过镜像启动容器，并把本机8099端口映射到容器8000端口
    docker run -d -v /var/log/:/var/log/ -p ${containerPort}:8000 --name ${containerName} ${imageName}
 
