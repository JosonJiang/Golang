#此脚本---用来判断是否运行了容器

#----------------------------------------------------------

#判断是否存在镜像
docker images | grep golangs &> /dev/null

#如果不存在，使用docker-compose启动相关的容器
if [ $? -ne 0 ]
then
    echo "golangs is not existed,we will docker build it!!!"
    docker build -t golangs .
else
    echo "golangs is existed!!!"
    docker rmi golangs:latest
fi

#---------------------------------------

#判断是否存在容器
docker ps | grep josongolang &> /dev/null

#判断容器是否启动状态
#docker inspect --format '{{.State.Running}}' josongolang;

containerName ="josongolang"
# 查看进程是否存在
exist=`docker inspect --format '{{.State.Running}}' ${containerName}`

if [ "${exist}" != "true" ]; then

    echo "josongolang is runing !!!"

fi


#如果不存在，使用docker-compose启动相关的容器
if [ $? -ne 0 ]
then
    echo "josongolang is not up,we will start up it!!!"
  

    docker stop  $(docker ps -aq --filter "name=josongolang")
    docker rm -f josongolang
    docker run -d -p 8099:8000 --name josongolang golangs

# docker-compose -f /var/lib/jenkins/workspace/oschina_selenium_docker/oschina_docker.yaml up -d

#sudo docker run -it --name zalenium -p 4444:4444 \
 #-v /var/run/docker.sock:/var/run/docker.sock \
 #-v zalenium-videos:/home/seluser/videos \
   #--privileged jamesz2011/selenium-chrome-firefox-v1 start &> /dev/null

else
    echo "josongolang is run !!!"
fi