<<EOF

监控docker容器运行状态脚本shell

cat > docker_monitor.sh 
*/1 * * * * sh /opt/docker_status/docker_monitor.sh JosonContainer

JosonContainer 这是容器名称 作为参数传给脚本 如果是固定的 可以直接写死

https://xbuba.com/questions/52469662

EOF

#!/bin/bash
#监控容器的运行状态

#容器名称  传入参数
containerName=$1
#当前时间
now=`date +"%Y-%m-%d %H:%M:%S"`

# 查看进程是否存在
exist=`docker inspect --format '{{.State.Running}}' ${containerName}`

if [ "${exist}" != "true" ]; then
    docker start ${containerName}
    #记录日志
    echo "${now} 重启docker容器，容器名称：${containerName}" >> /var/logs/docker/docker_monitor.log
fi


