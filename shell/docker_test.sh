<<EOF

    Shell脚本中"command not found"报错处理

    字符串的定义与赋值
    # 定义STR1变量，值为abc

    STR1 = "abc"(错误写法)
    STR1="abc"(正确写法)


    # 比较两个字符串是否相等中if语句的写法

    STR1="abc"
    STR2="abcd"

    if[$STR1=$STR2](错误写法)
    if [ $STR1 = $STR2 ](正确写法)



    if语句中如果不添加空格的话，"if["会被识别为一个未知的命令，在运行时就会报错："command not found"，添加正确的空格才能运行



    #相等
    STR1="abc"
    STR2="abcd"

    #if[$STR1=$STR2](错误写法) 
    if [[ $STR1 = $STR2 ]] ; then
    　　echo "A"
    else
    　　echo "AAA"
    fi



    #包含
    STR1A="abc"
    STR2A="abbcd abc ccc"

    result=$(echo $STR2A | grep "${STR1A}")
    echo $result
    if [[ "$result" != "" ]] ;then
    　　echo "B"
    else 
        echo "BBB"
    fi




    #包含
    STR1B="abc"
    STR2B="abbcd abc ccc"

    if [[ $STR2B =~ $STR1B ]] ; then
    　　echo "C"
    else
    　　echo "CCC"
    fi

EOF

#!/bin/bash


    # -s = Silent cURL's output
    # -L = Follow redirects
    # -w = Custom output format
    # -o = Redirects the HTML output to /dev/null

    curl -sL -w "%{http_code}\\n" "http://www.google.com/" -o /dev/null
    if [[ "200" == $? ]]; then  echo "可以访问Google"; else  echo "!!! Fuck !!! Google is Forbidden" ; fi


    #接收到传入值

    containerName=$1
    imageName=$2

    echo "接收到参数 $1 < containerName : ${containerName}, $2 < imageName : ${imageName}"



    #删除镜像

    # if [[ "$(docker images -q golangs:latest 2> /dev/null)" != "" ]]; then echo ”删除镜像 ${imageName}“ ; else echo ”没有镜像 ${imageName}“; fi
    # if [[ "$(docker images | grep golangs 2> /dev/null)" != "" ]]; then echo ”删除镜像Q ${imageName}“ ; else echo ”没有镜像Q ${imageName}“; fi
    # if [[ "$(docker images | grep golangs 2> /dev/null)" != "" || "$(docker images -q golangs:latest 2> /dev/null)" != "" ]]; then echo ”删除镜像X ${imageName}“ ; else echo ”没有镜像X ${imageName}“; fi




    if [[ "$(docker images -q ${imageName}:latest 2> /dev/null)" != "" ]]; then echo ”删除镜像 ${imageName}“ ; else echo ”没有镜像 ${imageName}“; fi
    if [[ "$(docker images | grep ${imageName} 2> /dev/null)" != "" ]]; then echo ”删除镜像Q ${imageName}“ ; else echo ”没有镜像Q ${imageName}“; fi

    if [[ "$(docker images | grep ${imageName} 2> /dev/null)" != "" || "$(docker images -q ${imageName}:latest 2> /dev/null)" != "" ]]; then echo ”删除镜像X ${imageName}“ ; else echo ”没有镜像X ${imageName}“; fi



    # 删除 TAG  <none>  的容器 判断
    #  - echo "删除TAG <none>的容器"

     if [[ -n `docker images | grep "none" | awk "{print $3}" 2> /dev/null` || -n `docker images -q -f dangling=true 2> /dev/null` ]] ; then
          
          echo "删除TAG <none>的容器X"
          #docker rmi $(docker images | grep "none" | awk '{print $3}')
     else      
          echo "没有TAG <none>的容器X"
     fi




     if [[ -n `docker images | grep "none" | awk "{print $3}"` || -n `docker images -q -f dangling=true` ]] ; then
          
          echo "删除TAG <none>的容器"
          #docker rmi $(docker images | grep "none" | awk '{print $3}')

     else      
          echo "没有TAG <none>的容器"
     fi




    # - docker rmi $(docker images -q -f dangling=true)



    # 停止容器  
    # if [[ -n "`docker ps -aq --filter "name=josongolang"`"  ||  -n "`docker ps -a  | grep "josongolang" | wc -l`" ]] ; then 
    if [[ -n "`docker ps -aq --filter "name=${containerName}"`"  ||  -n "`docker ps -a  | grep "${containerName}" | wc -l`" ]] ; then 
        echo "停止容器 ${containerName}"
       
        #docker stop  $(docker ps -aq --filter "name=josongolang")
        #docker rm -f josongolang
    else      
        echo "没有容器 ${containerName}"
    fi



    # 删除容器
 

    # 三种判断
    # 推荐使用第一种  ${var}

    # 拒绝使用第二种  因为 $(shell命令) 可能和 `shell命令` 混淆
    # 下面第二句 首先 `docker images -aq "golangs"` 得到变量，然后使用 $(变量) 判断的，注意理解

    # 第三种 $var


    # -n "${var}"
    # -n "$(var)"
    # -n "$var"

    docker ps -aq --filter "name=${containerName}"
    if [ -n $? ]; then  echo "删除容器 ${containerName}"; else  echo "没有容器 ${containerName}" ; fi


    docker images -aq "${imageName}"
    if [ -n $? ]; then  echo "删除镜像 ${imageName}"; else  echo "没有镜像 ${imageName}" ; fi


    # if [[ -n "`docker images -aq "golangs"`" ]]; then  echo "删除镜像A"; else  echo "没有镜像A" ; fi
    # if [[ -n "$(docker images -aq "golangs")" ]]; then  echo "删除镜像B"; else  echo "没有镜像B" ; fi
    # if [[ -n "$`docker images -aq "golangs"`" ]]; then  echo "删除镜像C"; else  echo "没有镜像C" ; fi


    if [[ -n "`docker images -aq "${imageName}"`" ]]; then  echo "删除镜像${imageName}A"; else  echo "没有镜像${imageName}A" ; fi
    if [[ -n "$(docker images -aq "${imageName}")" ]]; then  echo "删除镜像${imageName}B"; else  echo "没有镜像${imageName}B" ; fi
    if [[ -n "$`docker images -aq "${imageName}"`" ]]; then  echo "删除镜像${imageName}C"; else  echo "没有镜像${imageName}C" ; fi
